let balance = document.getElementById('balance');
let transaction = document.getElementById('transaction');


let btns = document.querySelectorAll('.btns');

const compute = (e)=>{
	let mobileNum = document.querySelector('#mobileNum').value;
	if (mobileNum == '' || mobileNum.length !== 11) {
		alert('Please enter a valid mobile number.')
	} else {
		let loadAmount = e.target.attributes.value.value;
		let loadBalance = balance.innerHTML - loadAmount;
		balance.innerHTML = loadBalance;
		transaction.innerHTML += `${loadAmount} is loaded to - ${mobileNum} <br/>`;
	}
};

for (let i = 0; i < btns.length; i++) {
	btns[i].addEventListener('click', compute);
};